<?php

namespace Drupal\table_of_contents;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\field\FieldConfigInterface;

/**
 * Contains helpers for the text formatters related to the table of contents.
 */
final class TocTextFieldHelper {

  use StringTranslationTrait;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  private $messenger;

  /**
   * The field types that should have a table of contents computed in them.
   *
   * @todo: make this list configurable by the user.
   */
  const TABLE_OF_CONTENTS_FIELD_TYPES = [
    'text_long',
    'text_with_summary',
  ];

  /**
   * TocTextFormatterHelpers constructor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * Checks weather the plugin belongs to a long text field or not.
   *
   * @param \Drupal\field\FieldConfigInterface $field
   *   The field.
   *
   * @return bool
   *   TRUE if the formatter is for a long text field.
   */
  public function fieldIsSupported(FieldConfigInterface $field) {
    return $this->fieldTypeIsSupported($field->getType());
  }

  /**
   * Checks weather the field type belongs to a long text field or not.
   *
   * @param string $field_type
   *   The field type.
   *
   * @return bool
   *   TRUE if the formatter is for a long text field.
   */
  public function fieldTypeIsSupported($field_type) {
    return in_array($field_type, static::TABLE_OF_CONTENTS_FIELD_TYPES);
  }

  /**
   * Executes during field config pre save.
   *
   * @param \Drupal\field\FieldConfigInterface $field
   *   The field config.
   */
  public function onFieldConfigPreSave(FieldConfigInterface $field) {
    $needs_rebuild = $field->getThirdPartySetting('table_of_contents', 'toc_block', FALSE);
    $original_field = $field->original;
    if ($original_field instanceof FieldConfigInterface) {
      $was_toc_enabled = $original_field
        ->getThirdPartySetting('table_of_contents', 'toc_block', FALSE);
      // Use of != and instead of !== is intentional.
      $needs_rebuild = $needs_rebuild != $was_toc_enabled;
    }

    if ($needs_rebuild) {
      block_rebuild();
      $message = $this->t('Blocks were rebuilt to reflect the new configuration for the <em>Table of Contents</em>.');
      $this->messenger->addMessage($message);
    }
  }

  /**
   * Generates the form definition for the formatter settings.
   *
   * @param \Drupal\field\FieldConfigInterface $plugin
   *   The plugin.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form definition.
   */
  public function getTocSettingsForm(FieldConfigInterface $plugin, FormStateInterface $form_state) {
    $link = Link::createFromRoute($this->t('block administration'), 'block.admin_display')->toString();
    return [
      '#type' => 'details',
      '#title' => $this->t('Flexible Table of Contents'),
      '#description' => $this->t('Enable and configure the block with the table of contents for this field.'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#weight' => -10,
      '#submit' => ['block_rebuild'],
      'toc_block' => [
        '#title' => $this->t('Enable the TOC block for this field'),
        '#description' => $this->t('Enable a block that will contain the table of contents for this field. Activating this will make it available in the @link page.', ['@link' => $link]),
        '#type' => 'checkbox',
        '#default_value' => $plugin->getThirdPartySetting('table_of_contents', 'toc_block', FALSE),
      ],
      'toc_selector' => [
        '#title' => $this->t('Enter a CSS selector'),
        '#description' => $this->t('The matched elements will be used as the header items. Example: <em>h3</em>.'),
        '#type' => 'textfield',
        '#default_value' => $plugin->getThirdPartySetting('table_of_contents', 'toc_selector', 'h2'),
      ],
    ];
  }

}
