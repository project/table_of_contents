<?php

namespace Drupal\table_of_contents\Plugin\Block;

use Drupal\Component\Utility\Crypt;
use Drupal\Component\Utility\Html;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\field\FieldConfigInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\CssSelector\CssSelectorConverter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block that renders the TOC for a field from an entity.
 *
 * @Block(
 *   id = "text_long_field_toc_block",
 *   deriver = "\Drupal\table_of_contents\Plugin\Derivative\TextLongFieldTocBlockDeriver",
 *   context = {
 *     "language" = @ContextDefinition("language", label = @Translation("Language"))
 *   }
 * )
 *
 * @internal
 *   Plugin classes are internal.
 */
final class TextLongFieldTocBlock extends BlockBase implements ContextAwarePluginInterface, ContainerFactoryPluginInterface {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  private $entityFieldManager;

  /**
   * The entity type ID.
   *
   * @var string
   */
  private $entityTypeId;

  /**
   * The bundle ID.
   *
   * @var string
   */
  private $bundle;

  /**
   * The field name.
   *
   * @var string
   */
  private $fieldName;

  /**
   * The field definition.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface
   */
  private $fieldDefinition;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private $logger;

  /**
   * Constructs a new FieldBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityFieldManagerInterface $entity_field_manager, LoggerInterface $logger) {
    $this->entityFieldManager = $entity_field_manager;
    $this->logger = $logger;

    // Get the entity type and field name from the plugin ID.
    list (, $entity_type_id, $bundle, $field_name) = explode(static::DERIVATIVE_SEPARATOR, $plugin_id, 4);
    $this->entityTypeId = $entity_type_id;
    $this->bundle = $bundle;
    $this->fieldName = $field_name;

    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_field.manager'),
      $container->get('logger.channel.table_of_contents')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    try {
      $entity = $this->getEntity();
      // Fields can be multivalue. We will concatenate the contents.
      $field_definition = $entity->get($this->fieldName)->getFieldDefinition();
      if (!$field_definition instanceof FieldConfigInterface) {
        return NULL;
      }
      $toc_items = $this->findTocItems($field_definition, $entity);
      if ($toc_items === FALSE || !$toc_items->length) {
        // If there are no TOC items, do not display the block.
        return NULL;
      }
      $build = $this->buildTocListItems(
        $toc_items,
        $field_definition->getThirdPartySetting('table_of_contents', 'toc_selector', 'h2')
      );
    }
    catch (\Exception $e) {
      $this->logger->warning('The field "%field" failed to render with the error of "%error".', ['%field' => $this->fieldName, '%error' => $e->getMessage()]);
      return NULL;
    }
    CacheableMetadata::createFromObject($this)->applyTo($build);
    return $build;
  }

  /**
   * Builds the list items render array with the linked table of contents.
   *
   * @param \DOMNodeList $toc_items
   *   The list of nodes.
   * @param string $css_selector
   *   The CSS selector for the table of content items.
   *
   * @return array
   *   The render array containing the table of contents.
   */
  private function buildTocListItems(\DOMNodeList $toc_items, $css_selector) {
    $items = [];
    foreach ($toc_items as $toc_item) {
      assert($toc_item instanceof \DOMElement);
      $classes = ['toc-link'];
      if ($toc_item->hasAttribute('id')) {
        $id = $toc_item->getAttribute('id');
      }
      else {
        $id = $this->generateDomNodeId($toc_item);
        $classes[] = 'toc-link-invalid-id';
      }
      $items[] = [
        '#type' => 'link',
        '#title' => $toc_item->textContent,
        '#url' => Url::fromRoute('<none>', [], ['fragment' => $id]),
        '#attributes' => ['class' => $classes],
      ];
    }
    return [
      '#theme' => 'item_list__table_of_contents',
      '#items' => $items,
      '#attributes' => ['class' => ['anchor-list']],
      '#attached' => [
        'library' => ['table_of_contents/table_of_contents.anchors'],
        'drupalSettings' => [
          'tableOfContents' => [
            [
              'fieldName' => Html::getClass($this->fieldName),
              'selector' => $css_selector,
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * Finds the TOC items in the dom.
   *
   * @param \Drupal\field\FieldConfigInterface $field_definition
   *   The field definition that contains the content for the TOC.
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity that contains the field with the content.
   *
   * @return \DOMNodeList|false
   *   The list of elements. FALSE if none found.
   */
  private function findTocItems(FieldConfigInterface $field_definition, FieldableEntityInterface $entity) {
    $field_storage_definition = $field_definition->getFieldStorageDefinition();
    $main_property_name = $field_storage_definition->getMainPropertyName();
    $values = $entity->get($this->fieldName)->getValue();
    $html = array_reduce($values, function ($carry, $value) use ($main_property_name) {
      $markup = empty($value[$main_property_name]) ? '' : $value[$main_property_name];
      if (!empty($value['format'])) {
        $markup = (string) check_markup($markup, $value['format']);
      }
      return $carry . $markup;
    }, '');
    if (empty($html)) {
      return FALSE;
    }
    $dom = Html::load($html);
    $css_selector = $field_definition->getThirdPartySetting('table_of_contents', 'toc_selector', 'h2');
    $xpath_query = (new CssSelectorConverter(TRUE))->toXPath($css_selector, 'descendant-or-self::');
    $xpath = new \DOMXpath($dom);
    return $xpath->query($xpath_query);
  }

  /**
   * Generates an ID for anchoring if there is none.
   *
   * @param \DOMNode $node
   *   The dom element.
   *
   * @return string
   *   The generated ID.
   */
  private function generateDomNodeId(\DOMNode $node) {
    $text = $node->textContent;
    // We can't use drupal_html_id here since it keeps track of ids on the page,
    // and we're having to use check_markup on certain input formats. This is a
    // bit hacky, but it'll have to do for now.
    $id = strtr(mb_strtolower($text), [
      ' ' => '-',
      '_' => '-',
      '[' => '-',
      ']' => '',
    ]);
    // Remove any punctuation.
    $id = preg_replace('/[^A-Za-z0-9\-_]/', '', $id);
    // Make sure there the id is not too long and unique.
    $id = substr($id, 0, 10) . '-' . substr(Crypt::hashBase64($text), 0, 8);
    $id = 'toc-' . $id;
    return $id;
  }

  /**
   * Gets the entity that has the field.
   *
   * @return \Drupal\Core\Entity\FieldableEntityInterface
   *   The entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  private function getEntity() {
    return $this->getContextValue('entity');
  }

  /**
   * {@inheritdoc}
   */
  public function getPreviewFallbackString() {
    return new TranslatableMarkup('Table of contents for field "@field"', ['@field' => $this->getFieldDefinition()->getLabel()]);
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    $entity = $this->getEntity();

    // First consult the entity.
    $access = $entity->access('view', $account, TRUE);
    if (!$access->isAllowed()) {
      return $access;
    }

    // Check that the entity in question has this field.
    if (!$entity instanceof FieldableEntityInterface || !$entity->hasField($this->fieldName)) {
      return $access->andIf(AccessResult::forbidden());
    }

    // Check field access.
    $field = $entity->get($this->fieldName);
    $access = $access->andIf($field->access('view', $account, TRUE));
    if (!$access->isAllowed()) {
      return $access;
    }

    // Check to see if the field has any values.
    if ($field->isEmpty()) {
      return $access->andIf(AccessResult::forbidden());
    }
    return $access;
  }

  /**
   * Gets the field definition.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface
   *   The field definition.
   */
  private function getFieldDefinition() {
    if (empty($this->fieldDefinition)) {
      $field_definitions = $this->entityFieldManager->getFieldDefinitions($this->entityTypeId, $this->bundle);
      $this->fieldDefinition = $field_definitions[$this->fieldName];
    }
    return $this->fieldDefinition;
  }

}
